///<reference path="../Geometry/Line.ts"/>
///<reference path="Drawer.ts"/>
///<reference path="../Logic/Engine.ts"/>
var Graphics;
(function (Graphics) {
    var Animation = (function () {
        function Animation() {
        }
        Animation.animate = function (data) {
            var debugCanvas = $('#debugCanvas')[0];
            var debugContext = debugCanvas.getContext('2d');
            var mainCanvas = $('#myCanvas')[0];
            var mainContext = mainCanvas.getContext('2d');
            var step = 4000;
            Graphics.Drawer.clear(800, 600, mainContext);
            _.each(data.groups, function (group, index) {
                _.delay(function () {
                    _.delay(function () {
                        debugContext.strokeStyle = 'rgba(255, 255, 0, 0.5)';
                        Graphics.Drawer.segment(group.pair.previous, debugContext);
                    }, step * 0.1);
                    _.delay(function () {
                        Graphics.Drawer.segment(group.pair.next, debugContext);
                    }, step * 0.1);
                    _.delay(function () {
                        debugContext.strokeStyle = 'rgba(0, 0, 255, 0.5)';
                        Graphics.Drawer.segment(group.projectedRay, debugContext);
                    }, step * 0.2);
                    _.delay(function () {
                        debugContext.strokeStyle = 'rgba(0, 0, 255, 0.5)';
                        Graphics.Drawer.segment(group.projectedEdge, debugContext);
                    }, step * 0.2);
                    _.delay(function () {
                        debugContext.strokeStyle = 'rgb(0, 255, 255)';
                        Graphics.Drawer.segment(group.projectedPair.previous, debugContext);
                    }, step * 0.1);
                    _.delay(function () {
                        Graphics.Drawer.segment(group.projectedPair.next, debugContext);
                    }, step * 0.1);
                    _.delay(function () {
                        Graphics.Drawer.clear(800, 600, debugContext);
                        mainContext.fillStyle = "rgba(255, 255, 255, 0.5)";
                        Graphics.Drawer.fillPolygon(new Polygon([group.projectedPair.next.end, group.projectedPair.previous.end, data.source.position]), mainContext);
                    }, step * 0.5);
                }, (index + 1) * step);
            });
            _.delay(function () {
                var vertices = _.reduce(data.groups, function (memo, group) {
                    memo.push(group.projectedPair.previous.end);
                    memo.push(group.projectedPair.next.end);
                    return memo;
                }, []);
                vertices = _.uniq(vertices, function (v) { return v.stringify(); });
                Graphics.Drawer.clear(800, 600, mainContext);
                Graphics.Drawer.fillPolygon(new Polygon(vertices), mainContext);
            }, step * (data.groups.length + 1));
        };
        return Animation;
    })();
    Graphics.Animation = Animation;
})(Graphics || (Graphics = {}));
//# sourceMappingURL=Animation.js.map
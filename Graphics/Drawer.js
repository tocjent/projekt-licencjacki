/// <reference path="../declarations/lodash.d.ts"/>
/// <reference path="../Geometry/Point.ts"/>
/// <reference path="../Geometry/Line.ts"/>
/// <reference path="../Geometry/Circle.ts"/>
/// <reference path="../Geometry/Polygon.ts"/>
/// <reference path="../Logic/Ray.ts"/>
/// <reference path="../Logic/Engine.ts"/>
/// <reference path="../declarations/jquery.d.ts"/>
/**
 * Created by Michal on 2014-10-30.
 */
var Polygon = Geometry.Polygon;
var Circle = Geometry.Circle;
var Graphics;
(function (Graphics) {
    var Drawer = (function () {
        function Drawer() {
        }
        Drawer.clear = function (width, height, context) {
            context.clearRect(0, 0, width, height);
        };
        Drawer.draw = function (shape, context) {
            if (shape instanceof Polygon) {
                Drawer.polygon(shape, context);
            }
            else if (shape instanceof LineSegment) {
                Drawer.segment(shape, context);
            }
            else if (shape instanceof LineSegment) {
                Drawer.circle(shape, context);
            }
            else if (shape instanceof Logic.Ray) {
                Drawer.ray(shape, context);
            }
        };
        Drawer.polygon = function (polygon, context) {
            context.beginPath();
            context.moveTo(polygon.last.x, polygon.last.y);
            _.each(polygon.vertices, function (vertex, index) {
                context.lineTo(vertex.x, vertex.y);
            });
            context.closePath();
            context.stroke();
        };
        Drawer.fillPolygon = function (polygon, context) {
            context.beginPath();
            context.moveTo(polygon.last.x, polygon.last.y);
            _.each(polygon.vertices, function (vertex) { return context.lineTo(vertex.x, vertex.y); });
            context.closePath();
            context.fill();
        };
        Drawer.segment = function (segment, context) {
            context.beginPath();
            context.moveTo(segment.start.x, segment.start.y);
            context.lineTo(segment.end.x, segment.end.y);
            context.closePath();
            context.stroke();
        };
        Drawer.ray = function (ray, context) {
            context.beginPath();
            context.moveTo(Logic.Ray.start.x, Logic.Ray.start.y);
            context.lineTo(ray.end.x, ray.end.y);
            context.closePath();
            context.stroke();
        };
        Drawer.circle = function (circle, context) {
            context.beginPath();
            context.arc(circle.pos.x, circle.pos.y, circle.radius, 0, 2 * Math.PI);
            context.closePath();
            context.stroke();
        };
        Drawer.fillCircle = function (circle, context) {
            context.beginPath();
            context.arc(circle.pos.x, circle.pos.y, circle.radius, 0, 2 * Math.PI);
            context.closePath();
            context.fill();
        };
        Drawer.light = function (data, context) {
            var vertices = _.reduce(data.groups, function (memo, group) {
                memo.push(group.projectedPair.previous.end);
                memo.push(group.projectedPair.next.end);
                return memo;
            }, []);
            vertices = _.uniq(vertices, function (v) { return v.stringify(); });
            Graphics.Drawer.clear(800, 600, context);
            context.fillStyle = "rgba(255, 0, 255, 0.3)";
            var gradient = context.createRadialGradient(data.source.position.x, data.source.position.y, 10, data.source.position.x, data.source.position.y, 400);
            gradient.addColorStop(0, 'rgba(255, 255, 255, 0.8)');
            gradient.addColorStop(1, 'rgba(0, 0, 0, 0.1)');
            context.fillStyle = gradient;
            Graphics.Drawer.fillPolygon(new Polygon(vertices), context);
        };
        return Drawer;
    })();
    Graphics.Drawer = Drawer;
})(Graphics || (Graphics = {}));
//# sourceMappingURL=Drawer.js.map
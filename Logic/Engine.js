var Logic;
(function (Logic) {
    Logic.update = function (sourcePosition, size, edges, shapes, callback) {
    };
    var createRays = function (edges) {
        ////stworz promien z kazdego wierzcholka, usun duplikaty, posortuj wedlug kata
        var mapByProp = R.curry(function (propName, classArg) { return R.map(R.pipe(R.prop(propName), function (e) { return R.constructN(1, classArg)(e); })); });
        var joinRays = function (edges) { return R.unionWith(R.eqProps('stringify'), mapByProp('start')(Logic.Ray)(edges), mapByProp('end')(Logic.Ray)(edges)); };
        return R.pipe(joinRays, R.sortBy(R.prop('angle')))(edges);
    };
    var pairRays = function (rays) {
        //sparuj kolejne bedace obok siebie promienie
        var shift = function (array) { return R.prepend(R.last(array), R.init(array)); };
        var pair = function (array) { return R.zipWith(function (x, y) { return ({ previous: x, next: y }); }, shift(array), array); };
        return pair(rays);
    };
    var projectRays = function (pairs) {
        //dla kazdej pary promieni rzutuj promien miedzy nimi
        var angleBetween = R.memoize(function (a, b) {
            var angle = (a + b) / 2;
            angle += Math.abs(a - b) > Math.PI ? Math.PI : 0;
            return angle;
        });
        var createRay = R.curry(function (propA, propB, propC, x) { return new Logic.Ray(new Point(Logic.Ray.start.x + 10000 * Math.cos(angleBetween(x[propA][propC], x[propB][propC])), Logic.Ray.start.y + 10000 * Math.sin(angleBetween(x[propA][propC], x[propB][propC])))); });
        return R.map(createRay('previous')('next')('angle'), pairs);
    };
    var projectedEdges = function (edges, projectedRays) {
        //dla kazdego rzutowanego promienia zwroc najblizsza sciane
    };
    var joinPairs = function (projectedEdges, pairs) {
        //połącz pary rzutowane na tę samą ścianę
    };
    var projectGroupRays = function (pairs) {
        //dla kazdej polaczonej pary rzutuj promien miedzy nimi
    };
    var groupData = function (arrays, propertyNames) {
        //połącz wszystkie dane w obiekt
    };
    var Engine = (function () {
        function Engine() {
        }
        Engine.update = function (sourcePosition, edges, shapes, callback) {
            Logic.Ray.start = sourcePosition;
            var rays = createRays(edges);
            var pairs = pairRays(rays);
            var projectedRays = projectRays(pairs);
            var projectedEdges = Logic.Engine.projectedEdges(edges, projectedRays);
            var projectedPairs = Logic.Engine.projectPairs(projectedEdges, pairs);
            var groups = Logic.Engine.groupData(_.zip(rays, pairs, projectedRays, projectedEdges, projectedPairs), ['ray', 'pair', 'projectedRay', 'projectedEdge', 'projectedPair']);
            var projectedRayGroups = Logic.Engine.projectGroupRays(groups);
            return callback({
                source: {
                    position: sourcePosition,
                    radius: Engine.size
                },
                edges: edges,
                shapes: shapes,
                groups: groups
            });
        };
        Engine.createRays = function (edges) {
            return _(edges).map(function (e) { return [new Logic.Ray(e.start), new Logic.Ray(e.end)]; }).flatten().uniq(function (r) { return r.stringify; }).sortBy(function (r) { return r.angle; }).value();
        };
        Engine.pairRays = function (rays) {
            var shifted = [];
            shifted.push(_.last(rays));
            _.each(_.initial(rays), function (r) { return shifted.push(r); });
            var pairs = _.zip(shifted, rays);
            return _.map(pairs, function (p) {
                return { previous: p[0], next: p[1] };
            });
        };
        Engine.projectRays = function (pairs) {
            return _.map(pairs, function (r) {
                var projectedAngle = (r.previous.angle + r.next.angle) / 2;
                projectedAngle += Math.abs(r.previous.angle - r.next.angle) > Math.PI ? Math.PI : 0;
                return new Logic.Ray(new Point(Logic.Ray.start.x + 10000 * Math.cos(projectedAngle), Logic.Ray.start.y + 10000 * Math.sin(projectedAngle)));
            });
        };
        Engine.projectedEdges = function (edges, projectedRays) {
            return _.map(projectedRays, function (r) {
                return _.chain(edges).map(function (e) { return _.extend(e, { intersection: LineSegment.intersect(r, e) }); }).filter(function (e) { return e.intersection.result; }).map(function (e) { return _.extend(e, { distance: Point.distance(e.intersection.pos, Logic.Ray.start) }); }).reject(function (e) { return e.distance === 0; }).min(function (e) { return e.distance; }).value();
            });
        };
        Engine.projectPairs = function (projectedEdges, pairs) {
            var groups = _.map(_.zip(projectedEdges, pairs), function (g) {
                return { edge: g[0], previous: g[1].previous, next: g[1].next };
            });
            return _.map(groups, function (g) {
                return {
                    previous: new Logic.Ray(Geometry.LineSegment.intersect(g.edge, g.previous).pos),
                    next: new Logic.Ray(Geometry.LineSegment.intersect(g.edge, g.next).pos)
                };
            });
        };
        Engine.projectGroupRays = function (groups) {
            return _.map(groups, function (group) {
                var r = group.pair;
                var projectedAngle = (r.previous.angle + r.next.angle) / 2;
                projectedAngle += Math.abs(r.previous.angle - r.next.angle) > Math.PI ? Math.PI : 0;
                group.projectedRay = new Logic.Ray(new Point(Logic.Ray.start.x + 10000 * Math.cos(projectedAngle), Logic.Ray.start.y + 10000 * Math.sin(projectedAngle)));
                return group;
            });
        };
        Engine.groupData = function (arrays, propNames) {
            var groups = _.map(arrays, function (a) {
                return _.zipObject(propNames, a);
            });
            groups = _.reduce(groups, function (memo, group) {
                if (memo.length === 0 || group.projectedEdge.stringify !== _.last(memo).projectedEdge.stringify) {
                    memo.push(group);
                }
                else if (group.projectedEdge.stringify === _.last(memo).projectedEdge.stringify) {
                    _.last(memo).pair.next = group.pair.next;
                    _.last(memo).projectedPair.next = group.projectedPair.next;
                }
                return memo;
            }, []);
            return groups;
        };
        Engine.size = 10;
        return Engine;
    })();
    Logic.Engine = Engine;
})(Logic || (Logic = {}));
//# sourceMappingURL=Engine.js.map
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="../Geometry/Point.ts"/>
/// <reference path="../Geometry/Line.ts"/>
/// <reference path="../Geometry/Drawable.ts"/>
/**
 * Created by Michal on 2014-11-10.
 */
var LineSegment = Geometry.LineSegment;
var Point = Geometry.Point;
var Logic;
(function (Logic) {
    var Ray = (function (_super) {
        __extends(Ray, _super);
        function Ray(end) {
            _super.call(this, Ray.start, end);
            this.angle = Ray.angleBetween(Ray.start, end);
        }
        Ray.angleBetween = function (a, b) {
            return Math.atan2(b.y - a.y, b.x - a.x);
        };
        return Ray;
    })(LineSegment);
    Logic.Ray = Ray;
})(Logic || (Logic = {}));
//# sourceMappingURL=Ray.js.map
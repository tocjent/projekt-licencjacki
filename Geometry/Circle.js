///<reference path="../declarations/lodash.d.ts"/>
///<reference path="Point.ts"/>
///<reference path="Polygon.ts"/>
/**
 * Created by Michal on 2014-11-11.
 */
var Geometry;
(function (Geometry) {
    var Circle = (function () {
        function Circle(pos, radius) {
            this._pos = pos;
            this._radius = radius;
        }
        Object.defineProperty(Circle.prototype, "radius", {
            get: function () {
                return this._radius;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Circle.prototype, "pos", {
            get: function () {
                return this._pos;
            },
            enumerable: true,
            configurable: true
        });
        Circle.prototype.approx = function () {
            var vertices = new Array();
            var accuracy = Math.round(10);
            var angle = 2 * Math.PI * ((accuracy - 1) / accuracy);
            vertices.push(new Geometry.Point(this._pos.x + this._radius * Math.cos(angle), this._pos.y + this._radius * Math.sin(angle)));
            var pos = this.pos;
            var radius = this.radius;
            _.times(accuracy, function (index) {
                angle = 2 * Math.PI * ((index) / accuracy);
                vertices.push(new Geometry.Point(pos.x + radius * Math.cos(angle), pos.y + radius * Math.sin(angle)));
            });
            return new Geometry.Polygon(vertices);
        };
        Circle.prototype.edges = function () {
            var edges = new Array();
            _.each(this.approx().edges(), function (edge) {
                edges.push(edge);
            });
            return edges;
        };
        return Circle;
    })();
    Geometry.Circle = Circle;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Circle.js.map
/// <reference path="Edgeable.ts"/>
/// <reference path="Drawable.ts"/>
/// <reference path="Point.ts"/>
/**
 * Created by Michal on 2014-10-30.
 */
var Geometry;
(function (Geometry) {
    var LineSegment = (function () {
        function LineSegment(start, end) {
            this._start = start;
            this._end = end;
            this.stringify = String(this._start.x) + ',' + String(this._start.y) + ',' + String(this._end.x) + ',' + String(this._end.y);
        }
        Object.defineProperty(LineSegment.prototype, "start", {
            get: function () {
                return this._start;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LineSegment.prototype, "end", {
            get: function () {
                return this._end;
            },
            enumerable: true,
            configurable: true
        });
        LineSegment.intersect = function (line1, line2) {
            var denominator, a, b, numerator1, numerator2;
            denominator = ((line2.end.y - line2.start.y) * (line1.end.x - line1.start.x)) - ((line2.end.x - line2.start.x) * (line1.end.y - line1.start.y));
            if (denominator == 0) {
                return {
                    pos: new Geometry.Point(line1.start.x + (a * (line1.end.x - line1.start.x)), line1.start.y + (a * (line1.end.y - line1.start.y))),
                    result: false
                };
            }
            a = line1.start.y - line2.start.y;
            b = line1.start.x - line2.start.x;
            numerator1 = ((line2.end.x - line2.start.x) * a) - ((line2.end.y - line2.start.y) * b);
            numerator2 = ((line1.end.x - line1.start.x) * a) - ((line1.end.y - line1.start.y) * b);
            a = numerator1 / denominator;
            b = numerator2 / denominator;
            var onLine1 = a >= 0 && a <= 1;
            var onLine2 = b >= 0 && b <= 1;
            return {
                pos: new Geometry.Point(line1.start.x + (a * (line1.end.x - line1.start.x)), line1.start.y + (a * (line1.end.y - line1.start.y))),
                result: onLine1 && onLine2
            };
        };
        LineSegment.prototype.edges = function () {
            var array = new Array();
            array.push(new LineSegment(this.start, this.end));
            return array;
        };
        return LineSegment;
    })();
    Geometry.LineSegment = LineSegment;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Line.js.map
/**
 * Created by Michal on 2014-10-30.
 */
var Geometry;
(function (Geometry) {
    var Point = (function () {
        function Point(positionOrX, y) {
            if (typeof positionOrX === 'number') {
                this._x = Math.round(positionOrX);
                this._y = Math.round(y);
            }
            else {
                this._x = Math.round(positionOrX.x);
                this._y = Math.round(positionOrX.y);
            }
        }
        Object.defineProperty(Point.prototype, "x", {
            get: function () {
                return this._x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Point.prototype, "y", {
            get: function () {
                return this._y;
            },
            enumerable: true,
            configurable: true
        });
        Point.distance = function (a, b) {
            return Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2);
        };
        Point.prototype.stringify = function () {
            return String(this.x) + ',' + String(this.y);
        };
        return Point;
    })();
    Geometry.Point = Point;
    ;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Point.js.map
///<reference path="Line.ts"/>
///<reference path="Point.ts"/>
///<reference path="Edgeable.ts"/>
///<reference path="Drawable.ts"/>
/**
 * Created by tocjent on 31.10.14.
 */
var Geometry;
(function (Geometry) {
    var Polygon = (function () {
        function Polygon(vertices) {
            this._vertices = vertices;
        }
        Object.defineProperty(Polygon.prototype, "first", {
            get: function () {
                return this._vertices[0];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Polygon.prototype, "last", {
            get: function () {
                return this._vertices[this._vertices.length - 1];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Polygon.prototype, "vertexCount", {
            get: function () {
                return this._vertices.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Polygon.prototype, "vertices", {
            get: function () {
                return this._vertices;
            },
            enumerable: true,
            configurable: true
        });
        Polygon.prototype.edges = function () {
            var edges = Array();
            var previous = this.last;
            this.vertices.forEach(function (vertex) {
                edges.push(new Geometry.LineSegment(previous, vertex));
                previous = vertex;
            });
            return edges;
        };
        Polygon.rectangle = function (x, y, width, height) {
            var rectangle = new Array();
            rectangle.push(new Geometry.Point(x, y));
            rectangle.push(new Geometry.Point(x + width, y));
            rectangle.push(new Geometry.Point(x + width, y + height));
            rectangle.push(new Geometry.Point(x, y + height));
            return rectangle;
        };
        return Polygon;
    })();
    Geometry.Polygon = Polygon;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Polygon.js.map
/// <reference path="Line.ts"/>
/**
 * Created by Michal on 2014-11-11.
 */
module Geometry{
    export interface Edgeable{
        edges(): Array<Geometry.LineSegment>;
    }
}
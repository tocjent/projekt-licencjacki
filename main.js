/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/lodash.d.ts"/>
/// <reference path="Graphics/Drawer.ts"/>
/// <reference path="Graphics/Animation.ts"/>
/// <reference path="Geometry/Point.ts"/>
/// <reference path="Geometry/Line.ts"/>
/// <reference path="Logic/Engine.ts"/>
/**
 * Created by Michal on 2014-10-29.
 */
var windowWidth = 800;
var windowHeight = 600;
var shapes = new Array();
shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(20, 20, windowWidth - 40, windowHeight - 40)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(50, 50, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(50, 400, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(225, 50, 150, 150)));
shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(300, 100, 400, 400)));
//shapes.push(new LineSegment(new Point(100, 150), new Point(400, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(225, 225, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(400, 50, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(400, 400, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(575, 225, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(575, 400, 150, 150)));
//shapes.push(new Geometry.Polygon(Geometry.Polygon.rectangle(125, 125, 150, 150)));
var edges = _.reduce(shapes, function (memo, shape) { return _.union(memo, shape.edges()); }, new Array());
edges = _.reduce(edges, function (memo, e) {
    var intersections = _.chain(edges).map(function (cross) { return Geometry.LineSegment.intersect(e, cross); }).filter(function (cross) { return cross.result; }).sortBy(function (cross) { return (Math.pow((cross.pos.x - e.start.x), 2) + Math.pow((cross.pos.y - e.start.y), 2)); }).map(function (cross) { return cross.pos; }).value();
    if (intersections.length > 0) {
        var previousEnd = e.start;
        _.each(intersections, function (cross) {
            memo.push(new Geometry.LineSegment(previousEnd, cross));
            previousEnd = cross;
        });
        memo.push(new Geometry.LineSegment(previousEnd, _.last(intersections)));
    }
    else {
        memo.push(e);
    }
    return memo;
}, new Array());
$(document).ready(function () {
    var canvas = $('#background')[0];
    var ctx = canvas.getContext('2d');
    $('#animate').click(function () {
        Logic.Engine.update(new Geometry.Point(_.random(0, 800), _.random(0, 600)), edges, shapes, Graphics.Animation.animate);
    });
    $('#menu').css('left', windowWidth);
    $('canvas').prop('width', windowWidth);
    $('canvas').prop('height', windowHeight);
    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillRect(0, 0, windowWidth, windowHeight);
    ctx.strokeStyle = "rgb(0, 255, 0)";
    _.each(shapes, function (s) { return Graphics.Drawer.draw(s, ctx); });
    canvas = $('#myCanvas')[0];
    ctx = canvas.getContext('2d');
    $(canvas).prop('width', windowWidth);
    $(canvas).prop('height', windowHeight);
    //$(canvas).mousemove(handler);
    ctx.strokeStyle = "rgb(0, 0, 255)";
    handler({ clientX: windowWidth / 2, clientY: windowHeight / 2 });
    var rotation = 0;
    setInterval(function () {
        Logic.Engine.update(new Geometry.Point(position(windowWidth / 2, windowHeight / 2, 250, rotation)), edges, shapes, draw);
        rotation = rotation < 1 ? rotation + 0.001 : 0;
    }, 10);
});
function draw(data) {
    var canvas = $('#myCanvas')[0];
    var ctx = canvas.getContext('2d');
    Graphics.Drawer.clear(windowWidth, windowHeight, ctx);
    Graphics.Drawer.light(data, ctx);
}
function position(x, y, radius, rotation) {
    return {
        x: x + radius * Math.cos(rotation * 2 * Math.PI),
        y: y + radius * Math.sin(rotation * 2 * Math.PI)
    };
}
;
function handler(event) {
    if (event.clientX >= 40 && event.clientX <= windowWidth - 40 && event.clientY >= 40 && event.clientY <= windowHeight - 40) {
        Logic.Engine.update(new Geometry.Point(event.clientX, event.clientY), edges, shapes, draw);
    }
}
;
//# sourceMappingURL=main.js.map